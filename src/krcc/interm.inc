      COMMON/INTER/
! pointers *8
* H22 --10--> M12
     &KNH22TOM12,KNH22T12,KCOMBNH22TOM12,KPERMNH22TOM12,
     &KFACNH22TOM12,KCOMBFACNH22TOM12,KSNH22TOM12,KFNH22TOM12,
* H22 --02--> M20
     &KNH22TOM20,KNH22T20,KCOMBNH22TOM20,KPERMNH22TOM20,
     &KFACNH22TOM20,KCOMBFACNH22TOM20,KSNH22TOM20,KFNH22TOM20,
* H22 --11--> M11
     &KNH22TOM11,KNH22T11,KCOMBNH22TOM11,KPERMNH22TOM11,
     &KFACNH22TOM11,KCOMBFACNH22TOM11,KSNH22TOM11,KFNH22TOM11,
* H22 --20--> M02
     &KNH22TOM02,KNH22T02,KCOMBNH22TOM02,KPERMNH22TOM02,
     &KFACNH22TOM02,KCOMBFACNH22TOM02,KSNH22TOM02,KFNH22TOM02,
* H22 --12--> M10
     &KNH22TOM10,KNH22T10,KCOMBNH22TOM10,KPERMNH22TOM10,
     &KFACNH22TOM10,KCOMBFACNH22TOM10,KSNH22TOM10,KFNH22TOM10,
* H22 --21--> M01
     &KNH22TOM01,KNH22T01,KCOMBNH22TOM01,KPERMNH22TOM01,
     &KFACNH22TOM01,KCOMBFACNH22TOM01,KSNH22TOM01,KFNH22TOM01,
* H22 --22--> T
     &KNH22TOT,KNH22TT,KCOMBNH22TOT,KPERMNH22TOT,
     &KFACNH22TOT,KCOMBFACNH22TOT,KSNH22TOT,KFNH22TOT,
* H21 --10--> M11
     &KNH21TOM11,KNH21T11,KCOMBNH21TOM11,KPERMNH21TOM11,
     &KFACNH21TOM11,KCOMBFACNH21TOM11,KSNH21TOM11,KFNH21TOM11,
* H21 --11--> M10
     &KNH21TOM10,KNH21T10,KCOMBNH21TOM10,KPERMNH21TOM10,
     &KFACNH21TOM10,KCOMBFACNH21TOM10,KSNH21TOM10,KFNH21TOM10,
* H21 --20--> M01
     &KNH21TOM01,KNH21T01,KCOMBNH21TOM01,KPERMNH21TOM01,
     &KFACNH21TOM01,KCOMBFACNH21TOM01,KSNH21TOM01,KFNH21TOM01,
* H21 --21--> T
     &KNH21TOT,KNH21TT,KCOMBNH21TOT,KPERMNH21TOT,
     &KFACNH21TOT,KCOMBFACNH21TOT,KSNH21TOT,KFNH21TOT,
* H12 --10--> M02
     &KNH12TOM02,KNH12T02,KCOMBNH12TOM02,KPERMNH12TOM02,
     &KFACNH12TOM02,KCOMBFACNH12TOM02,KSNH12TOM02,KFNH12TOM02,
* H12 --02--> M10
     &KNH12TOM10,KNH12T10,KCOMBNH12TOM10,KPERMNH12TOM10,
     &KFACNH12TOM10,KCOMBFACNH12TOM10,KSNH12TOM10,KFNH12TOM10,
* H12 --11--> M01
     &KNH12TOM01,KNH12T01,KCOMBNH12TOM01,KPERMNH12TOM01,
     &KFACNH12TOM01,KCOMBFACNH12TOM01,KSNH12TOM01,KFNH12TOM01,
* H12 --12--> T
     &KNH12TOT,KNH12TT,KCOMBNH12TOT,KPERMNH12TOT,
     &KFACNH12TOT,KCOMBFACNH12TOT,KSNH12TOT,KFNH12TOT,
* H20 --10--> M10
     &KNH20TOM10,KNH20T10,KCOMBNH20TOM10,KPERMNH20TOM10,
     &KFACNH20TOM10,KCOMBFACNH20TOM10,KSNH20TOM10,KFNH20TOM10,
* H20 --20--> T
     &KNH20TOT,KNH20TT,KCOMBNH20TOT,KPERMNH20TOT,
     &KFACNH20TOT,KCOMBFACNH20TOT,KSNH20TOT,KFNH20TOT,
* H11 --10--> M01
     &KNH11TOM01,KNH11T01,KCOMBNH11TOM01,KPERMNH11TOM01,
     &KFACNH11TOM01,KCOMBFACNH11TOM01,KSNH11TOM01,KFNH11TOM01,
* H11 --11--> T
     &KNH11TOT,KNH11TT,KCOMBNH11TOT,KPERMNH11TOT,
     &KFACNH11TOT,KCOMBFACNH11TOT,KSNH11TOT,KFNH11TOT,
* H02 --01--> M01
     &KNH02TOM01,KNH02T01,KCOMBNH02TOM01,KPERMNH02TOM01,
     &KFACNH02TOM01,KCOMBFACNH02TOM01,KSNH02TOM01,KFNH02TOM01,
* H02 --02--> T
     &KNH02TOT,KNH02TT,KCOMBNH02TOT,KPERMNH02TOT,
     &KFACNH02TOT,KCOMBFACNH02TOT,KSNH02TOT,KFNH02TOT,
* H10 --10--> T
     &KNH10TOT,KNH10TT,KCOMBNH10TOT,KPERMNH10TOT,
     &KFACNH10TOT,KCOMBFACNH10TOT,KSNH10TOT,KFNH10TOT,
* H01 --01--> T
     &KNH01TOT,KNH01TT,KCOMBNH01TOT,KPERMNH01TOT,
     &KFACNH01TOT,KCOMBFACNH01TOT,KSNH01TOT,KFNH01TOT,
* M12 --10--> M02
     &KNM12TOM02,KNM12T02,KCOMBNM12TOM02,KPERMNM12TOM02,
     &KFACNM12TOM02,KCOMBFACNM12TOM02,KSNM12TOM02,KFNM12TOM02,
* M20 --20--> T
     &KNM20TOT,KNM20TT,KCOMBNM20TOT,KPERMNM20TOT,
     &KFACNM20TOT,KCOMBFACNM20TOT,KSNM20TOT,KFNM20TOT,
* M11 --10--> M01
     &KNM11TOM01,KNM11T01,KCOMBNM11TOM01,KPERMNM11TOM01,
     &KFACNM11TOM01,KCOMBFACNM11TOM01,KSNM11TOM01,KFNM11TOM01,
* M11 --11--> T
     &KNM11TOT,KNM11TT,KCOMBNM11TOT,KPERMNM11TOT,
     &KFACNM11TOT,KCOMBFACNM11TOT,KSNM11TOT,KFNM11TOT,
* M02 --01--> M01
     &KNM02TOM01,KNM02T01,KCOMBNM02TOM01,KPERMNM02TOM01,
     &KFACNM02TOM01,KCOMBFACNM02TOM01,KSNM02TOM01,KFNM02TOM01,
* M02 --02--> T
     &KNM02TOT,KNM02TT,KCOMBNM02TOT,KPERMNM02TOT,
     &KFACNM02TOT,KCOMBFACNM02TOT,KSNM02TOT,KFNM02TOT,
* M10 --10--> T
     &KNM10TOT,KNM10TT,KCOMBNM10TOT,KPERMNM10TOT,
     &KFACNM10TOT,KCOMBFACNM10TOT,KSNM10TOT,KFNM10TOT,
* M01 --01--> T
     &KNM01TOT,KNM01TT,KCOMBNM01TOT,KPERMNM01TOT,
     &KFACNM01TOT,KCOMBFACNM01TOT,KSNM01TOT,KFNM01TOT,
* Matching H and T operator that can be added
     &KM12TOH12,KM11TOH11,KM02TOH02,KM10TOH10,KM01TOH01,KM00TOH00,
! Integers
* H22 --10--> M12
     &NH22TOM12,
* H22 --02--> M20
     &NH22TOM20,
* H22 --11--> M11
     &NH22TOM11,
* H22 --20--> M02
     &NH22TOM02,
* H22 --12--> M10
     &NH22TOM10,
* H22 --21--> M01
     &NH22TOM01,
* H22 --22--> T
     &NH22TOT,
* H21 --10--> M11
     &NH21TOM11,
* H21 --11--> M10
     &NH21TOM10,
* H21 --20--> M01
     &NH21TOM01,
* H21 --21--> T
     &NH21TOT,
* H12 --10--> M02
     &NH12TOM02,
* H12 --02--> M10
     &NH12TOM10,
* H12 --11--> M01
     &NH12TOM01,
* H12 --12--> T
     &NH12TOT,
* H20 --10--> M10
     &NH20TOM10,
* H20 --20--> T
     &NH20TOT,
* H11 --10--> M01
     &NH11TOM01,
* H11 --11--> T
     &NH11TOT,
* H02 --01--> M01
     &NH02TOM01,
* H02 --02--> T
     &NH02TOT,
* H10 --10--> T
     &NH10TOT,
* H01 --01--> T
     &NH01TOT,
* M12 --10--> M02
     &NM12TOM02,
* M20 --20--> T
     &NM20TOT,
* M11 --10--> M01
     &NM11TOM01,
* M11 --11--> T
     &NM11TOT,
* M02 --01--> M01
     &NM02TOM01,
* M02 --02--> T
     &NM02TOT,
* M10 --10--> T
     &NM10TOT,
* M01 --01--> T
     & NM01TOT 
